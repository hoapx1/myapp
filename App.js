import React from "react";
import { StyleSheet } from "react-native";
import Navigation from "./src/navigation";

const App = () => {
  return <Navigation />;
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "red",
  },
});

export default App;
