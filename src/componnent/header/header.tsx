import { useNavigation } from "@react-navigation/native";
import React from "react";
import { Image, Text, View, StyleSheet } from "react-native";
import { TouchableOpacity } from "react-native";
// import MenuIcon from "@mui/icons-material/Menu";

const Header = () => {
  return (
    <View style={styles.header}>
      {/* <MenuIcon /> */}
      <View>
        <Text style={styles.headerText}>Header</Text>
      </View>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  header: {
    width: "100%",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  headerText: {
    fontWeight: "bold",
    fontSize: 20,
    color: "#333",
    letterSpacing: 1,
  },
});
