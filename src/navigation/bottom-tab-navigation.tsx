import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import React from "react";
import HomeScreen from "../screen/home/home";
import Setting from "../screen/setting/setting";
import HomeStackScreen from "./home-navigation";
import SettingStackScreen from "./setting-navigation";

const Tab = createBottomTabNavigator();
const MainStack = () => {
  return (
    <Tab.Navigator screenOptions={{ headerShown: false }}>
      <Tab.Screen name="home" component={HomeScreen} />
      <Tab.Screen name="setting" component={Setting} />
    </Tab.Navigator>
  );
};

export default MainStack;
