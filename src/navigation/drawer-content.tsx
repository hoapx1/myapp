import React from "react";
import {
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from "@react-navigation/drawer";

const DrawerContent = (props) => {
  return (
    <DrawerContentScrollView {...props}>
      <DrawerItemList {...props} />
      <DrawerItem
        label="About"
        onPress={() => props.navigation.navigate("About")}
      />
      <DrawerItem
        label="Contact"
        onPress={() => props.navigation.navigate("Contact")}
      />
    </DrawerContentScrollView>
  );
};

export default DrawerContent;
