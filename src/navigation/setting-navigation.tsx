import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Setting from "../screen/setting/setting";

const SettingStack = createNativeStackNavigator();

const SettingStackScreen = () => {
  return (
    <SettingStack.Navigator>
      <SettingStack.Screen name="Setting" component={Setting} />
    </SettingStack.Navigator>
  );
};

export default SettingStackScreen;
