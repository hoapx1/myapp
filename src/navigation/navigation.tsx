import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import LoginScreen from "../screen/login/logIn";
import SignInScreen from "../screen/signIn/sign-in";
import CreateNewAccount from "../screen/create-new-account/create-new-account";

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
// import LogInStackScreen from "../navigation/log-in-navigator";
import HomeStackScreen from "./home-navigation";
import Setting from "../screen/setting/setting";
import SettingStackScreen from "./setting-navigation";
import DrawerStackNavigation from "./drawer-stack-navigation";
import SignInStackScreen from "./sign-in-navigator";
import { Button } from "react-native";

const Stack = createNativeStackNavigator();

const Navigation = () => {
  return (
    <Stack.Navigator
      initialRouteName="DrawerStackNavigation"
      screenOptions={{ headerShown: false }}
    >
      {/* <Stack.Screen name="SignInStackScreen" component={SignInStackScreen} /> */}
      {/* <Stack.Screen name="MainStack" component={MainStack} /> */}
      <Stack.Screen
        name="DrawerStackNavigation"
        component={DrawerStackNavigation}
      />
    </Stack.Navigator>
  );
};

export default Navigation;
