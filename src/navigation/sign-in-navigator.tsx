import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import LoginScreen from "../screen/login/logIn";
import SignInScreen from "../screen/signIn/sign-in";
import CreateNewAccount from "../screen/create-new-account/create-new-account";
import DrawerStackNavigation from "./drawer-stack-navigation";

const SignInStack = createNativeStackNavigator();

const SignInStackScreen = () => {
  return (
    <SignInStack.Navigator screenOptions={{ headerShown: false }}>
      <SignInStack.Screen name="LogIn" component={LoginScreen} />
      <SignInStack.Screen name="SignIn" component={SignInScreen} />
      <SignInStack.Screen
        name="DrawerStackNavigation"
        component={DrawerStackNavigation}
      />
    </SignInStack.Navigator>
  );
};

export default SignInStackScreen;
