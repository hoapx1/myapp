import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import HomeScreen from "../screen/home/home";
import Setting from "../screen/setting/setting";
import Animated from "react-native-reanimated";
import MainStack from "./bottom-tab-navigation";
import Store from "../screen/store/store";

const Drawer = createDrawerNavigator();

const DrawerStackNavigation = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Home" component={MainStack} />
      <Drawer.Screen name="Setting" component={Setting} />
      <Drawer.Screen name="Store" component={Store} />
    </Drawer.Navigator>
  );
};

export default DrawerStackNavigation;
