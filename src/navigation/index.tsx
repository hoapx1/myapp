import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import LoginStack from "./navigation";
import HomeStackScreen from "./home-navigation";
import { DrawerContent } from "@react-navigation/drawer";
import DrawerStackNavigation from "./drawer-stack-navigation";
import Navigation from "./navigation";

const RootComponent = () => {
  return (
    <NavigationContainer>
      <Navigation />
    </NavigationContainer>
  );
};
export default RootComponent;
