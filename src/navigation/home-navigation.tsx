import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import HomeScreen from "../screen/home/home";
import Setting from "../screen/setting/setting";
import Header from "../componnent/header/header";
import MainStack from "./bottom-tab-navigation";
import About from "../screen/about/about";
import Contact from "../screen/contact/contact";

const HomeStack = createNativeStackNavigator();

const HomeStackScreen = () => {
  return (
    <HomeStack.Navigator screenOptions={{ headerShown: false }}>
      <HomeStack.Screen name="Tab" component={MainStack} />
      <HomeStack.Screen name="About" component={About} />
      <HomeStack.Screen name="Contact" component={Contact} />
    </HomeStack.Navigator>
  );
};

export default HomeStackScreen;
