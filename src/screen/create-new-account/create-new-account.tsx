import React from 'react';
import {
  Button,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';

const CreateNewAccount = ({navigation}) => {
  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{paddingHorizontal: 15}}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{width: 30}}>
          <Image
            source={require('../../componnent/themes/arrow-icon-left.png')}
            resizeMode="stretch"
            style={{width: 30, height: 30}}
          />
        </TouchableOpacity>
        <Text
          style={{
            fontSize: 30,
            fontWeight: 'bold',
            marginTop: 30,
            marginLeft: 20,
          }}>
          Create new account
        </Text>
        <TextInput
          style={{
            borderWidth: 1,
            padding: 15,
            borderRadius: 20,
            marginTop: 50,
            marginHorizontal: 20,
          }}
          placeholder="Full Name"
        />
        <TextInput
          style={{
            borderWidth: 1,
            padding: 15,
            borderRadius: 20,
            marginTop: 20,
            marginHorizontal: 20,
          }}
          placeholder="Phone Name"
        />
        <TextInput
          style={{
            borderWidth: 1,
            padding: 15,
            borderRadius: 20,
            marginTop: 20,
            marginHorizontal: 20,
          }}
          placeholder="E-mail Address"
        />
        <TextInput
          style={{
            borderWidth: 1,
            padding: 15,
            borderRadius: 20,
            marginTop: 20,
            marginHorizontal: 20,
          }}
          placeholder="Password"
        />
        <View
          style={{
            display: 'flex',
            alignItems: 'center',
          }}>
          <View
            style={{
              backgroundColor: '#5c5db8',
              marginVertical: 30,
              borderWidth: 1,
              padding: 5,
              borderRadius: 20,
              width: 250,
            }}>
            <Button title="Sign Up" color="white" />
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default CreateNewAccount;
