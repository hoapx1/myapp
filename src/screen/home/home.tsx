import { useNavigation } from "@react-navigation/native";
import React from "react";
import { ImageSourcePropType } from "react-native";
import {
  TouchableOpacity,
  Dimensions,
  StatusBar,
  Text,
  View,
  Image,
  StyleSheet,
  FlatList,
  Animated,
} from "react-native";
import {
  Directions,
  FlingGestureHandler,
  State,
} from "react-native-gesture-handler";
import { SafeAreaView } from "react-native-safe-area-context";
import DATA from "./data";

const width = Dimensions.get("screen").width;
const ITEM_WIDTH = width * 0.8;
const ITEM_HEIGHT = ITEM_WIDTH * 1.2;
const VISIBLE_ITEM = 3;

// interface ImageType {
//   title: string;
//   image?: ImageSourcePropType;
// }

const HomeScreen = ({ navigation }) => {
  const data = DATA;
  const scrollXIndex = React.useRef<Animated.Value>(
    new Animated.Value(0)
  ).current;
  const scrollXAnimated = React.useRef(new Animated.Value(0)).current;
  const [index, setIndex] = React.useState<number>(0);

  // let a: Array<number | string> = [1, 2, 3,"heheh"];

  // let a: ImageType = {
  //   title: "string",
  // };

  // let b: Array<ImageType> = [a];

  // let c = 0;

  // let d = c as string

  React.useEffect(() => {
    Animated.spring(scrollXAnimated, {
      toValue: scrollXIndex,
      duration: 300,
      useNativeDriver: true,
    }).start();
  }, []);

  const setActiveIndex = React.useCallback((activeIndex) => {
    setIndex(activeIndex);
    scrollXIndex.setValue(activeIndex);
  }, []);

  return (
    <FlingGestureHandler
      key="LEFT"
      direction={Directions.LEFT}
      onHandlerStateChange={({ nativeEvent }) => {
        if (nativeEvent.state === State.END) {
          if (index === data.length - 1) {
            return;
          }
          setActiveIndex(index + 1);
        }
      }}
    >
      <FlingGestureHandler
        key="RIGHT"
        direction={Directions.RIGHT}
        onHandlerStateChange={({ nativeEvent }) => {
          if (nativeEvent.state === State.END) {
            if (index === 0) {
              return;
            }
            setActiveIndex(index - 1);
          }
        }}
      >
        <View style={{ flex: 1 }}>
          <StatusBar hidden />
          <FlatList
            data={data}
            scrollEnabled={false}
            removeClippedSubviews={false}
            horizontal
            inverted
            CellRendererComponent={({
              index,
              item,
              children,
              style,
              ...props
            }) => {
              const newStyle = [
                style,
                {
                  zIndex: data.length - index,
                  left: -ITEM_WIDTH / 2,
                  top: -ITEM_HEIGHT / 1.2,
                },
              ];
              return (
                <View index={index} {...props} style={newStyle}>
                  {children}
                </View>
              );
            }}
            contentContainerStyle={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
            }}
            keyExtractor={(_, index) => String(index)}
            renderItem={({ item, index }) => {
              const inputRange = [index - 1, index, index + 1];
              const translateX = scrollXAnimated.interpolate({
                inputRange,
                outputRange: [50, 0, -100],
              });
              const scale = scrollXAnimated.interpolate({
                inputRange,
                outputRange: [0.8, 1, 1.3],
              });
              const opacity = scrollXAnimated.interpolate({
                inputRange,
                outputRange: [1 - 1 / VISIBLE_ITEM, 1, 0],
              });
              return (
                <Animated.View
                  style={{
                    position: "absolute",
                    opacity,
                    transform: [{ translateX }, { scale }],
                  }}
                >
                  <TouchableOpacity>
                    <Image source={{ uri: item.poster }} style={styles.image} />
                    <View
                      style={{ position: "absolute", bottom: 20, left: 20 }}
                    >
                      <Text style={styles.text}>{item.title}</Text>
                    </View>
                  </TouchableOpacity>
                </Animated.View>
              );
            }}
          />
        </View>
      </FlingGestureHandler>
    </FlingGestureHandler>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  image: {
    width: ITEM_WIDTH,
    height: ITEM_HEIGHT,
    resizeMode: "cover",
    borderRadius: 16,
  },
  text: {
    textTransform: "uppercase",
    color: "#fff",
    fontSize: 30,
    fontWeight: "900",
  },
});
