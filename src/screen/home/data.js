const DATA = [
  {
    title: "Messi",
    poster:
      "https://i.pinimg.com/474x/11/20/41/1120419a12e05b15d840eac0897512fb.jpg",
  },
  {
    title: "Pogba",
    poster:
      "https://i.pinimg.com/474x/66/c3/3b/66c33bfdb55d2985eade31ed49b8a8c8.jpg",
  },
  {
    title: "Kevin De Bruyne",
    poster:
      "https://i.pinimg.com/474x/9b/22/de/9b22ded95ac198d43d2fa1f1cf88970f.jpg",
  },
  {
    title: "Son Heung Min",
    poster:
      "https://i.pinimg.com/474x/e3/e1/3d/e3e13d99d963e7c3837634f38831b1eb.jpg",
  },
  {
    title: "Ngolo Kante",
    poster:
      "https://i.pinimg.com/474x/e3/88/5b/e3885b3943e439c1fb0a35b5f0c778ec.jpg",
  },
  {
    title: "David Beckham",
    poster:
      "https://i.pinimg.com/474x/d7/5c/4e/d75c4e25366d30b37f40ccac54fb8b0f.jpg",
  },
  {
    title: "Cristiano Ronaldo",
    poster:
      "https://i.pinimg.com/474x/72/7a/10/727a10c49048bd62d747aebac72d5fd9.jpg",
  },
  {
    title: "Neyma JR",
    poster:
      "https://i.pinimg.com/474x/f9/09/0e/f9090eeba59b3afa7e4c24cbdbea453b.jpg",
  },
];
export default DATA;
