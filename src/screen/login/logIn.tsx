import React from 'react';
import {Text, View, Button} from 'react-native';
import {useStyleHelp} from './logIn-style';

const LoginScreen = ({navigation}) => {
  const {styles} = useStyleHelp();
  return (
    <View style={styles.CONTAINER}>
      <View style={styles.VIEW_TEXT}>
        <Text style={{fontSize: 120, fontWeight: 'bold'}}>S</Text>
        <Text style={{fontSize: 30, fontWeight: 'bold', marginTop: 30}}>
          Welcome to Shopertino
        </Text>
        <Text style={{fontSize: 15, marginTop: 30, alignSelf: 'center'}}>
          Shop & get updates on new products and sales with our mobile app
        </Text>
        <View
          style={{
            marginTop: 30,
            backgroundColor: 'black',
            borderRadius: 20,
            padding: 5,
            width: 250,
          }}>
          <Button
            title="Log In"
            color="white"
            onPress={() => navigation.navigate('SignIn')}
          />
        </View>
        <View
          style={{
            marginTop: 30,
            borderWidth: 1,
            padding: 5,
            borderRadius: 20,
            width: 250,
          }}>
          <Button
            title="Sign Up"
            color="black"
            onPress={() => navigation.navigate('SignUp')}
          />
        </View>
      </View>
    </View>
  );
};

export default LoginScreen;
