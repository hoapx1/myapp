import {StyleSheet} from 'react-native';

export const useStyleHelp = () => {
  const styles = StyleSheet.create({
    CONTAINER: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    VIEW_TEXT: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      paddingHorizontal: 30,
    },
  });
  return {styles};
};
