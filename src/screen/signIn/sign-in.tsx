import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  Button,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";

const SignInScreen = ({ navigation }) => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ paddingHorizontal: 15 }}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{ width: 30 }}
        >
          <Image
            source={require("../../componnent/themes/arrow-icon-left.png")}
            resizeMode="stretch"
            style={{ width: 30, height: 30 }}
          />
        </TouchableOpacity>
        <Text
          style={{
            fontSize: 35,
            fontWeight: "bold",
            marginTop: 30,
            marginLeft: 20,
          }}
        >
          Sign In
        </Text>

        <TextInput
          style={{
            borderWidth: 1,
            padding: 15,
            borderRadius: 20,
            marginTop: 50,
            marginHorizontal: 20,
          }}
          placeholder="E-mail or phone number"
        />
        <TextInput
          style={{
            borderWidth: 1,
            padding: 15,
            borderRadius: 20,
            marginTop: 20,
            marginHorizontal: 20,
          }}
          placeholder="Password"
        />
        <View
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <View
            style={{
              backgroundColor: "black",
              marginVertical: 30,
              borderWidth: 1,
              padding: 5,
              borderRadius: 20,
              width: 250,
            }}
          >
            <Button
              title="Log In"
              color="white"
              onPress={() => navigation.navigate("MainStack")}
            />
          </View>
          <Text style={{ fontSize: 20, fontWeight: "bold" }}> OR</Text>
          <View
            style={{
              backgroundColor: "#5c5db8",
              marginTop: 30,
              borderWidth: 1,
              padding: 5,
              borderRadius: 20,
              width: 250,
            }}
          >
            <Button title="Facebook Login" color="white" />
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default SignInScreen;
